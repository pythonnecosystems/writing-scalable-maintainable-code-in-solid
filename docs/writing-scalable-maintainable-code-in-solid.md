# SOLID 원칙: 확장 가능하고 유지 관리 가능한 코드 작성 

## 1. SOLID의 'S'는 단일 책임을 의미한다.

![](./images/1_enX4PJGzy647ibd-GpSkBQ.webp)

이 원칙은 우리에게 다음을 알려준다.

> <font size=5>*코드를 하나의 책임을 갖는 각각 모듈로 나눈다.*</font>

이메일 전송과 세금 계산과 같이 서로 관련이 없는 작업을 수행하는 `Person` 클래스를 살펴보자.

```python
class Person:
  def __init__(self, name, age):
    self.name = name
    self.age = age
  
    def send_email(self, message):
        # Code to send an email to the person
        print(f"Sending email to {self.name}: {message}")

    def calculate_tax(self):
        # Code to calculate tax for the person
        tax = self.age * 100
        print(f"{self.name}'s tax: {tax}")
```

**단일 책임 원칙(Single Responsibility Principle)**에 따라 원칙을 위반하지 않도록 `Person` 클래스를 여러 개의 작은 클래스로 분할해야 한다.

```python
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

class EmailSender:
    def send_email(person, message):
        # Code to send an email to the person
        print(f"Sending email to {person.name}: {message}")

class TaxCalculator:
    def calculate_tax(person):
        # Code to calculate tax for the person
        tax = person.age * 100
        print(f"{person.name}'s tax: {tax}")
```

물론 줄이 늘어났지만 이제 코드의 각 섹션이 달성하려는 목표를 더 쉽게 식별하고, 더 깔끔하게 테스트하고, 관련 없는 메서드에 대한 걱정 없이 코드의 일부를 다른 곳에 재사용할 수 있다.

## 2. 다음 'O'... 또는 개방/폐쇄 원리이다.

![](./images/1_xWwO2v121J8_FaBDmD_aUg.webp)

이 원칙에 따라 모듈을 설계하면 다음과 같이 할 수 있다.

> <font size=5>*기존 코드를 직접 수정하지 않고도 향후 새로운 기능을 추가할 수 있습니다.*</font>

모듈이 사용되면 기본적으로 잠기므로 새로 추가할 때 코드가 손상될 가능성이 줄어든다.

이 원칙은 모순적인 특성으로 인해 5가지 원칙 중 가장 이해하기 어려운 원칙 중 하나이므로 예를 들어 설명하겠다.

```python
class Shape:
    def __init__(self, shape_type, width, height):
        self.shape_type = shape_type
        self.width = width
        self.height = height

    def calculate_area(self):
        if self.shape_type == "rectangle":
            # Calculate and return the area of a rectangle
        elif self.shape_type == "triangle":
            # Calculate and return the area of a triangle
```

위의 예에서 `Shape` 클래스는 `calculate_area()` 메서드 내에서 다양한 도형 유형을 직접 처리한다. 이는 기존 코드를 확장하는 대신 수정하기 때문에 **개방/폐쇄 원칙(Open/Closed Principle)**에 위배된다.

이 설계는 도형 타입이 추가될수록 `calculate_area()` 메서드가 더 복잡해지고 유지 관리가 어려워지기 때문에 문제가 될 수 있다. 이는 책임 분리 원칙에 위배되며 코드의 유연성과 확장성을 떨어뜨린다. 이 문제를 해결할 수 있는 한 방법을 살펴보겠다.

```python
class Shape:
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def calculate_area(self):
        pass

class Rectangle(Shape):
    def calculate_area(self):
        # Implement the calculate_area() method for Rectangle

class Triangle(Shape):
    def calculate_area(self):
        # Implement the calculate_area() method for Triangle
```

위의 예에서는 보다 구체적인 도형 클래스가 그 속성을 상속할 수 있도록 하는 것이 유일한 목적인 기본 클래스 Shape를 정의했다. 예를 들어, `Triangle` 클래스는 삼각형의 면적을 계산하고 반환하는 `calculate_area()` 메서드를 갖도록 확장된다.

**개방/폐쇄 원칙**에 따라 기존 `Shape` 클래스를 수정하지 않고도 새로운 도형을 추가할 수 있다. 이를 통해 핵심 구현을 변경하지 않고도 코드의 기능을 확장할 수 있다.

## 3. 이제 'L'은... Liskov Substitution Principle (LSP)

![](./images/1_2PqRROjYtpjevNO2R0oa3w.webp)

이 원칙에서 Liskov가 말하고자 하는 것은 기본적으로 다음과 같다.

> <font size=5>*서브클래스는 프로그램의 기능을 손상시키지 않으면서 슈퍼클래스와 상호 교환적으로 사용할 수 있어야 한다.*</font>

이제 이것이 실제로 무엇을 의미할까? `start_engine()`이라는 메서드가 있는 `Vehicle` 클래스를 생각해 보자.

```python
class Vehicle:
    def start_engine(self):
        pass

class Car(Vehicle):
    def start_engine(self):
        # Start the car engine
        print("Car engine started.")

class Motorcycle(Vehicle):
    def start_engine(self):
        # Start the motorcycle engine
        print("Motorcycle engine started.")
```

**Liskov Substitution Principle**에 따르면, Vehicle 클래스의 어떤 하위 클래스도 문제 없이 엔진을 시동할 수 있어야 한다.

하지만 예를 들어 `Bicycle` 클래스를 추가했다고 가정해 보면, bicycle에는 엔진이 없으므로 더 이상 엔진을 시동할 수 없을 것이다. 아래는 이 문제를 해결하는 잘못된 방법을 보여 주고 있다.

```python
class Bicycle(Vehicle):
    def ride(self):
        # Rides the bike
        print("Riding the bike.")

    def start_engine(self):
         # Raises an error
        raise NotImplementedError("Bicycle does not have an engine.")
```

LSP를 제대로 준수하기 위해 두 가지 방법을 사용할 수 있다. 첫 번째 방법을 살펴보자.

**Solution 1**: 모든 `Vehicle`의 서브클래스가 자신의 슈퍼클래스와 일관되게 동작하도록 하기 위해 (상속 없이) `Bicycle`을 자체 클래스로 만든다.

```python
class Vehicle:
    def start_engine(self):
        pass

class Car(Vehicle):
    def start_engine(self):
        # Start the car engine
        print("Car engine started.")

class Motorcycle(Vehicle):
    def start_engine(self):
        # Start the motorcycle engine
        print("Motorcycle engine started.")

class Bicycle():
    def ride(self):
        # Rides the bike
        print("Riding the bike.")
```

**Solution 2**: 슈퍼 클래스 `Vehicle`를 엔진이 있는 Vehicle와 엔진이 없는 Vehicle 두 클래스로 나눈다. 그러면 예상 동작을 변경하거나 예외를 도입하지 않고도 모든 하위 클래스를 해당 슈퍼클래스와 상호 교환하여 사용할 수 있다.

```python
class VehicleWithEngines:
    def start_engine(self):
        pass

class VehicleWithoutEngines:
    def ride(self):
        pass

class Car(VehicleWithEngines):
    def start_engine(self):
        # Start the car engine
        print("Car engine started.")

class Motorcycle(VehicleWithEngines):
    def start_engine(self):
        # Start the motorcycle engine
        print("Motorcycle engine started.")

class Bicycle(VehicleWithoutEngines):
    def ride(self):
        # Rides the bike
        print("Riding the bike.")
```

## 4. 다음 단계... 인터페이스 분리를 위한 'I'

![](./images/1_zEgWeTUeyWssTLnyXjBALA.webp)

일반적인 정의에 따르면 모듈이 사용하지 않는 기능에 대해 걱정하지 않아도 된다고 한다. 하지만 이는 약간 모호하다. 이 모호한 문장을 좀 더 구체적인 지침으로 바꾸어 보겠다.

> <font size=5>*클라이언트별 인터페이스가 범용 인터페이스보다 낫다. 즉, 클래스가 사용하지 않는 인터페이스에 의존하도록 강요해서는 안 된다는 뜻이다. 대신 더 작고 구체적인 인터페이스에 의존해야 한다.*</font>

`walk()`, `swim()` 및 `fly()` 같은 메서드가 있는 `Animal` 인터페이스가 있다고 가정해 보자.

```python
class Animal:
    def walk(self):
        pass

    def swim(self):
        pass

    def fly(self):
        pass
```

문제는 모든 동물이 이 모든 행동을 할 수 있는 것은 아니라는 것이다.

**예를 들어** 개는 수영이나 비행을 할 수 없으므로 동물 인터페이스에서 상속된 이 두 가지 메서드는 호출되지 않을 것이다.

```python
class Dog(Animal):
    # Dogs can only walk
    def walk(self):
        print("Dog is walking.")

class Fish(Animal):
    # Fishes can only swim
    def swim(self):
        print("Fish is swimming.")

class Bird(Animal):
    # Birds cannot swim
    def walk(self):
        print("Bird is walking.")

    def fly(self):
        print("Bird is flying.")
```

동물 인터페이스를 더 작고 구체적인 하위 카테고리로 세분화하여 각 동물에 필요한 기능을 정확하게 구성할 수 있도록 해야 한다.

```python
class Walkable:
    def walk(self):
        pass

class Swimmable:
    def swim(self):
        pass

class Flyable:
    def fly(self):
        pass

class Dog(Walkable):
    def walk(self):
        print("Dog is walking.")

class Fish(Swimmable):
    def swim(self):
        print("Fish is swimming.")

class Bird(Walkable, Flyable):
    def walk(self):
        print("Bird is walking.")

    def fly(self):
        print("Bird is flying.")
```

이렇게 하면 클래스가 필요한 인터페이스에만 의존하는 디자인을 구현하여 불필요한 종속성을 줄일 수 있다. 이는 각 모듈에 필요한 기능만 모킹할 수 있기 때문에 테스트할 때 특히 유용하다.

## 5. 종속성 반전의 'D'는 다음과 같다.

![](./images/1_quGa1hK4EiHlPAUb7EcsTg.webp)

이것은 설명하기 비교적 간단하다.

> <font size=5>*상위 수준 모듈은 하위 수준 모듈에 직접 의존해서는 안 된다. 대신 둘(인터페이스 또는 추상 클래스) 모두의 추상화에 의존해야 한다.*</font>

다시 한 번 예를 살펴보겠다. 보고서를 생성하는 `ReportGenerator` 클래스가 있다고 가정해 보자. 이 작업을 수행하려면 먼저 데이터베이스에서 데이터를 가져와야 한다.

```python
class SQLDatabase:
    def fetch_data(self):
        # Fetch data from a SQL database
        print("Fetching data from SQL database...")

class ReportGenerator:
    def __init__(self, database: SQLDatabase):
        self.database = database

    def generate_report(self):
        data = self.database.fetch_data()
        # Generate report using the fetched data
        print("Generating report...")
```

이 예에서 `ReportGenerator` 클래스는 구체적인 `SQLDatabase` 클래스에 직접 종속된다.

현재로서는 이 방식이 잘 작동하지만 MongoDB와 같은 다른 데이터베이스로 전환하고 싶다면 어떻게 해야 할까? 이렇게 긴밀하게 연결되어 있으면 `ReportGenerator` 클래스를 수정하지 않고는 데이터베이스를 바꾸기가 어려울 것이다.

의존성 반전 원칙(Dependency Inversion Principle)을 준수하기 위해 `SQLDatabase`와 `MongoDatabase` 클래스가 모두 의존할 수 있는 추상화(또는 인터페이스)를 도입할 수 있다.

```python
class Database():
    def fetch_data(self):
        pass

class SQLDatabase(Database):
    def fetch_data(self):
        # Fetch data from a SQL database
        print("Fetching data from SQL database...")

class MongoDatabase(Database):
    def fetch_data(self):
        # Fetch data from a Mongo database
        print("Fetching data from Mongo database...")
```

이제 `ReportGenerator` 클래스는 생성자를 통해 새로운 데이터베이스 인터페이스에 종속된다는 점에 유의하세요.

```python
class ReportGenerator:
    def __init__(self, database: Database):
        self.database = database

    def generate_report(self):
        data = self.database.fetch_data()
        # Generate report using the fetched data
        print("Generating report...")
```

이제 상위 수준 모듈(`ReportGenerator`)은 하위 수준 모듈(`SQLDatabase` 또는 `MongoDatabase`)에 직접 종속되지 않는다. 대신 둘 다 인터페이스(`Database`)에 종속된다.

**종속성 반전(Dependency Inversion)**은 모듈이 어떤 구현을 받는지 알 필요 없이 특정 입력을 받고 특정 출력을 반환한다는 것만 알면 된다는 것을 의미한다.

## 마치며

![](./images/1_Uj5STPDFeqNvmv32pdnRAA.webp)

요즘 온라인에서 SOLID 설계 원칙과 그 원칙이 세월의 시험을 어떻게 견뎌냈는지에 대한 많은 토론을 볼 수 있다. 프로그래밍, 클라우드 컴퓨팅, 머신 러닝 등 다양한 패러다임이 존재하는 현대 사회에서 SOLID는 여전히 유효할까?

개인적으로 저자는 SOLID 원칙이 항상 좋은 코드 설계의 기초가 될 것이라고 믿는다. 소규모 어플리케이션으로 작업할 때는 이러한 원칙의 이점이 분명하지 않을 수도 있지만, 대규모 프로젝트에서 작업하기 시작하면 코드 품질의 차이가 학습 노력의 가치보다 훨씬 더 커질 것이다. SOLID가 장려하는 모듈성으로 인해 이러한 원칙은 여전히 최신 소프트웨어 아키텍처의 기반이 되고 있으며, 개인적으로는 이러한 원칙이 조만간 바뀔 것이라고 생각하지 않는다.
