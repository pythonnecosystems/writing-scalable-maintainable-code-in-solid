# SOLID 원칙: 확장 가능하고 유지 관리 가능한 코드 작성 <sup>[1](#footnote_1)</sup>

"나쁜 코드를 작성한다"는 말을 들은 적이 있나요?

그런 적이 있다면 부끄러워할 필요가 없다. 우리 모두는 배우면서 결함이 있는 코드를 작성한다. 좋은 소식은 의지만 있다면 코드를 개선하는 것은 그리 어려운 일이 아니라는 것이다.

코드를 개선하는 가장 좋은 방법 중 하나는 좋은 프로그래밍 설계 원칙을 배우는 것이다. 프로그래밍 원칙은 더 나은 프로그래머가 되기 위한 일반적인 지침, 즉 코딩의 원초적인 철학이라고 할 수 있다. 이 세상에는 수많은 원칙이 있지만(너무 많다고 주장할 수도 있다), 여기서는 SOLID라는 약어로 불리는 5가지 필수 원칙을 설명할 것이다.

> **Note**: *예에서는 Python을 사용하지만 이 개념은 Java와 같은 다른 언어에도 쉽게 적용할 수 있다*.

<a name="footnote_1">1</a>: [The SOLID Principles: Writing Scalable & Maintainable Code](https://forreya.medium.com/the-solid-principles-writing-scalable-maintainable-code-13040ada3bca)를 편역한 것이다.
